<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role='')
    {
        $userRole = $request->user();

        $roles = ['admin','staff'];

        if($userRole && in_array($userRole->role, $roles)) {

            return $next($request);

        } else {

            return redirect('/');
        }

        
    }
}
