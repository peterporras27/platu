<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Barangay;
use App\Town;
use App\Cart;
use Validator;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required|string',
            'town_id' => 'required|integer',
            'barangay_id' => 'required|integer',
            'phone' => 'required|string'
        ];
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carts = Cart::where([
            ['user_id','=',auth()->user()->id],
            ['status','=','hold']
        ])->get();

        if ($carts->count()==0) {
            return redirect('/')->with('error','Cart is empty.');
        }

        $towns = Town::all();
        $barangays = null;

        if (auth()->user()->town_id) {
            $barangays = Barangay::where('town_id','=',auth()->user()->town_id)->get();
        }

        return view('public.cart',compact('carts','towns','barangays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            $errors = $validator->messages()->messages();
            $error_message = '';
            
            foreach($errors as $error => $msg) {
                $error_message .= '- '.$msg[0].'<br>';
            }

            return redirect('cart')->with('error', $error_message);
        }

        if ( auth()->user()->banned ) {
            return redirect('/')->with('error', 'Your account has been banned and will no longer avail our services.');
        }

        // pull all cart
        $carts = Cart::where([
            ['user_id','=',auth()->user()->id],
            ['status','=','hold']
        ])->get();

        if ($carts->count() == 0) {
            return redirect('/')->with('error', 'Cart is empty.');
        }

        if (!auth()->user()->verified_member) {
            return redirect('/verify')->with('error', 'Kindly verify your account.');       
        }

        $total = 0;
        $transaction_code = $this->randStr();

        foreach ($carts as $cart) {
            $cartTotal = $cart->price*$cart->quantity;
            $total = $cartTotal + $total;
            $cart->transaction_code = $transaction_code;
            $cart->status = 'pending';
            $cart->save();
        }

        $brgy = Barangay::find($request->input('barangay_id'));

        $trans = new Transaction;
        $trans->transaction_code = $transaction_code;
        $trans->first_name = $request->input('first_name');
        $trans->last_name = $request->input('last_name');
        $trans->address = $request->input('address');
        $trans->barangay_id = $request->input('barangay_id');
        $trans->town_id = $request->input('town_id');
        $trans->phone = $request->input('phone');
        $trans->payment_type = $request->input('payment_type');
        $trans->delivery_fee = $brgy->delivery_fee;
        $trans->total_price = $total+$brgy->delivery_fee;
        $trans->user_id = auth()->user()->id;
        $trans->status = 'pending';
        $trans->save();

        if ( $request->input('payment_type') == 'gcash' ) {
            return redirect($this->gcash($trans->id));
        }

        return redirect('/order/'.$trans->id)->with('success', 'Order successful!');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $townid = $request->input('id');
        $baragays = Barangay::where('town_id','=',$townid)->get(['id','name','delivery_fee']);

        return response()->json($baragays);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();
        return redirect()->route('cart.index')
                        ->with('success','Cart item deleted successfully');
    }

    public function randStr($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function gcash($transaction_id)
    {
        $trans = Transaction::find($transaction_id);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://g.payx.ph/payment_request',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'x-public-key' => 'pk_9101ee3eb85ead311cfd1e5223d9208a',
                'amount' => $trans->total_price,
                'description' => 'Payment for services rendered',
                'merchantlogourl' => asset('images/logo.jpg'),
                'customername' => $trans->first_name.' '.$trans->last_name,
                'customermobile' => $trans->phone,
                'customeremail' => $trans->user->email,
                'webhooksuccessurl' => route('success'),
                'webhookfailurl' => route('fail'),
                'redirectsuccessurl' => route('order',$trans->id),
                'redirectfailurl' => route('order',$trans->id)
            ),
        ));

        $response = json_decode(curl_exec($curl));

        curl_close($curl);
        $return_url = 'order/'.$trans->id;

        if ($response->success) {

            $trans->gcash_data = json_encode($response->data);
            $trans->gcash_request_id = $response->data->request_id;
            $trans->save();

            $return_url = $response->data->checkouturl;
        }

        return $return_url;
    }
}
