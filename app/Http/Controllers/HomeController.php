<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Images;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $orders = Transaction::orderBy('created_at', 'DESC')->paginate(20);
        $status = array(
            'hold' => '',
            'pending' => 'warning',
            'processing' => 'info',
            'delivery' => 'info',
            'delivered' => 'success',
            'failed' => 'danger'
        );

        return view('admin.transactions',compact('orders','status'));
    }

    public function sales(Request $request)
    {
        $date = $request->input('date') ? $request->input('date') : date('Y-m-d');
        $date = date('Y-m-d', strtotime($date));
        $barangay = [];
        $month = date('m');
        $year = date('Y');

        if ($request->input('month') && $request->input('year')) {

            $sales = Transaction::whereYear('created_at', '=', $request->input('year'))
              ->whereMonth('created_at', '=', $request->input('month'))
              ->where('status','delivered')
              ->get();

            $month = $request->input('month');
            $year = $request->input('year');

        } else {

            $sales = Transaction::whereDate('created_at','=',$date)
                ->where('status','delivered')
                ->get();
        }

        $monthly = [];
        $total = 0;
        $grand_total = 0;

        return view('admin.sales',compact('sales','barangay','monthly','total','grand_total','year','month'));
    }

    public function deleteImage($id)
    {

        $img = Images::find($id);

        $return = array(
            'error' => true,
            'message' => 'please try again',
        );

        if ($img) {

            if ( Storage::exists($img->filename) ) 
            {
                Storage::delete($img->filename);
            }

            $return['error'] = false;
            $return['message'] = 'Image removed successfully.';
            $img->delete();
        }

        return $return;
    }
}
