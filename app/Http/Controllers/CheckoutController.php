<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://g.payx.ph/payment_request',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'x-public-key' => 'pk_9101ee3eb85ead311cfd1e5223d9208a',
                'amount' => '100',
                'description' => 'Payment for services rendered',
                'merchantlogourl' => '',
                'customername' => '',
                'customermobile' => '',
                'customeremail' => '',
            ),
        ));

        $response = json_decode(curl_exec($curl));

        curl_close($curl);

        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        $trans = Transaction::where('gcash_request_id','=',$request->input('request_id'))->first();

        if ($trans) {
            $trans->gcash_success = $request->input('success');
            $trans->gcash_request_id = $request->input('request_id');
            $trans->gcash_amount = $request->input('amount');
            $trans->gcash_reference = $request->input('reference');
            $trans->gcash_response_message = $request->input('response_message');
            $trans->gcash_response_advise = $request->input('response_advise');
            $trans->gcash_timestamp = $request->input('timestamp');
            $trans->save();
        }

        // success - With value of 1
        // request_id - This is the same as the request_id and hash in the JSON return
        // amount - This is the sum of amount and fee from your API Request Parameter
        // reference - This is the generated reference # of the transaction
        // response_message - Response Message from the transaction
        // response_advise - Response Advise from the transaction
        // timestamp - Timestamp of the transaction
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function fail(Request $request)
    {
        // success - With value of 0
        // request_id - This is the same as the request_id and hash in the JSON return
        // amount - This is the sum of amount and fee from your API Request Parameter
        // reference - This is the generated reference # of the transaction
        // response_message - Response Message from the transaction
        // response_advise - Response Advise from the transaction
        // timestamp - Timestamp of the transaction

        $trans = Transaction::where('gcash_request_id','=',$request->input('request_id'))->first();
        
        if ($trans) {
            $trans->gcash_success = $request->input('success');
            $trans->gcash_request_id = $request->input('request_id');
            $trans->gcash_amount = $request->input('amount');
            $trans->gcash_reference = $request->input('reference');
            $trans->gcash_response_message = $request->input('response_message');
            $trans->gcash_response_advise = $request->input('response_advise');
            $trans->gcash_timestamp = $request->input('timestamp');
            $trans->save();
        }
    }

    
}
