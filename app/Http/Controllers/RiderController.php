<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Cart;
use App\User;
use Validator;
use Auth;

class RiderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->validation = [
            'status' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Transaction::where('rider_id','=',Auth::user()->id)->paginate(20);
        $status = array(
            'hold' => '',
            'pending' => 'warning',
            'processing' => 'info',
            'delivery' => 'info',
            'delivered' => 'success',
            'failed' => 'danger'
        );

        return view('rider.index',compact('orders','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trans = Transaction::find($id);

        if (!$trans) { return redirect('rider'); }
        if ($trans->rider_id != auth()->user()->id) { return redirect('rider'); }

        $carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();
        $fee = $trans->brgy->delivery_fee;

        return view('rider.show',compact('trans','carts','fee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trans = Transaction::find($id);

        if (!$trans) {
            return redirect('rider')->with('error', 'Order no longer exist.');
        }

        if ($trans->rider_id != auth()->user()->id) { return redirect('rider'); }

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('rider/'.$id)->with('error', $validator->messages());
        }
        
        $trans->status = $request->input('status');
        
        if ($request->input('reason')) {
            $trans->reason = $request->input('reason');
        }

        if ($request->hasFile('proof')) {

            $allowedfileExtension = ['jpg','png','jpeg','gif'];
            $file = $request->file('proof');

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $check = in_array( $extension, $allowedfileExtension );

            if($check) {

                $filename = $request->proof->store('uploads');
                if ($filename) {
                    $trans->proof = $filename;
                }
            }

        } else {

            if ( $trans->status == 'delivered' && is_null($trans->proof) ) {
                return redirect('rider/'.$id)->with('error', 'Kindly upload a proof of purchase image of the buyer.');
            }
        }

        $trans->save();

        $carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();

        foreach ($carts as $cart) {
            $cart->status = $request->input('status');
            $cart->save();
        }

        return redirect('rider/'.$id)->with('success','Order succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
