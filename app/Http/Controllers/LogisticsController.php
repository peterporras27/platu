<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barangay;
use App\Town;
use Validator;

class LogisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
        $this->validation = [
            'name' => 'required|string|max:255',
            'delivery_fee' => 'required|string|max:255',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['logistics'] = Town::paginate(10);

        return view('admin.logistics.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            $errors = $validator->messages()->messages();
            $error_message = '';
            
            foreach($errors as $error => $msg) {
                $error_message .= '- '.$msg[0].'<br>';
            }

            return redirect('logistics')->with('error', $error_message);
        }

        $logistic = new Town;
        $logistic->fill( $request->all() );
        $logistic->save();

        return redirect('logistics')->with('success','Town succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $logistic = Barangay::find($id);

        $return = [
            'error' => true,
            'message' => 'Please try again.'
        ];

        if (!$logistic) {
            return response()->json($return);
        }

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) {
            return response()->json($validator->messages());
        }
        
        $logistic->fill( $request->all() );
        $logistic->save();

        $return['error'] = false;
        $return['message'] = 'Logistics succesfully updated!';

        return response()->json($return);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $logistic = Town::find($id);

        if (!$logistic) 
        {
            return redirect('logistics')->with('error', 'Logistic no longer exist.');
        }

        $logistic->delete();

        return redirect('logistics')->with('success','Logistic succesfully removed!');

    }
}
