<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
    protected $fillable = [
        'name',
        'delivery_fee'
    ];

    public function barangays()
    {
        return $this->hasMany('App\Barangay','town_id');
    }
}
