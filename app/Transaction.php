<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'transactions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_code',
        'first_name',
        'last_name',
        'address',
        'province',
        'barangay',
        'phone',
        'total_price',
        'barangay_id',
        'town_id',
        'user_id',
        'status',
        'reason',
        'payment_type',
        'gcash_data',
    ];

    public function carts()
    {
        return Cart::where('transaction_code','=',$this->transaction_code)->get();
    }

    public function user() {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function rider() {
        return $this->hasOne(User::class,'id','rider_id');
    }

    public function brgy() {
        return $this->hasOne(Barangay::class,'id','barangay_id');
    }

    public function town() {
        return $this->hasOne(Barangay::class,'id','town_id');
    }
}
