<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_code')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('reason')->nullable();
            $table->decimal('total_price', 10, 2)->nullable();
            $table->integer('user_id')->nullable();
            $table->enum('status',['hold','pending','processing','delivery','delivered','failed'])->default('hold');
            $table->decimal('delivery_fee', 10, 2)->default(0.00);
            $table->integer('town_id')->nullable();
            $table->integer('barangay_id')->nullable();
            $table->integer('rider_id')->nullable();
            $table->string('proof')->nullable();
            $table->enum('payment_type',['cash_on_delivery','gcash'])->default('cash_on_delivery');
            $table->json('gcash_data')->nullable();
            $table->boolean('gcash_success')->default(false);
            $table->string('gcash_request_id')->nullable();
            $table->string('gcash_amount')->nullable();
            $table->string('gcash_reference')->nullable();
            $table->string('gcash_response_message')->nullable();
            $table->string('gcash_response_advise')->nullable();
            $table->string('gcash_timestamp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
