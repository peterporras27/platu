<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['name' => 'Herb Toasted','description' => ' ','price' => 30.00,'category_id' => 1],
            ['name' => 'Ham & Egg','description' => ' ','price' => 38.00,'category_id' => 1],
            ['name' => 'Tuna & Egg','description' => ' ','price' => 38.00,'category_id' => 1],
            ['name' => 'Cheese Omellette','description' => ' ','price' => 38.00,'category_id' => 1],
            ['name' => 'Grilled Butter & Cheese','description' => ' ','price' => 35.00,'category_id' => 1],

            ['name' => 'Cheese Burger','description' => ' ','price' => 45.00,'category_id' => 2],
            ['name' => 'Chicken Fijata','description' => ' ','price' => 50.00,'category_id' => 2],
            ['name' => 'Beef Taco','description' => ' ','price' => 50.00,'category_id' => 2],
            ['name' => 'Tuna Salad','description' => ' ','price' => 45.00,'category_id' => 2],
            ['name' => 'Grilled Pork','description' => ' ','price' => 50.00,'category_id' => 2],
            ['name' => 'Ham & Cheese','description' => ' ','price' => 45.00,'category_id' => 2],
            ['name' => 'Sunny Side-up Egg','description' => ' ','price' => 40.00,'category_id' => 2],
            ['name' => 'Hotdog & Cheese','description' => ' ','price' => 45.00,'category_id' => 2],

            ['name' => 'Iced Tea','description' => ' ','price' => 30.00,'category_id' => 3],
            ['name' => 'Pink Lemonade','description' => ' ','price' => 35.00,'category_id' => 3],
            ['name' => 'Cucumber Cooler','description' => ' ','price' => 35.00,'category_id' => 3],
            ['name' => 'Pineapple Juice','description' => ' ','price' => 37.00,'category_id' => 3],
            ['name' => 'Four Seasons','description' => ' ','price' => 37.00,'category_id' => 3],

            ['name' => 'Mais Con Yelo','description' => ' ','price' => 40.00,'category_id' => 4],
            ['name' => 'Saging de Leche','description' => ' ','price' => 40.00,'category_id' => 4],
            ['name' => 'Cucumber Shake','description' => ' ','price' => 45.00,'category_id' => 4],
            ['name' => 'Apple Shake','description' => ' ','price' => 45.00,'category_id' => 4],
            ['name' => 'Apple Cucumber Shake','description' => ' ','price' => 50.00,'category_id' => 4],
            ['name' => 'Coffee Frost','description' => ' ','price' => 40.00,'category_id' => 4],

            ['name' => 'Meke Guisado - (Single)','description' => 'But may share by two persons','price' => 60.00,'category_id' => 5],
            ['name' => 'Bihon Guisado - (Single)','description' => 'But may share by two persons','price' => 65.00,'category_id' => 5],
            ['name' => 'Sotanghon Guisado - (Single)','description' => 'But may share by two persons','price' => 70.00,'category_id' => 5],
            ['name' => 'Chicken Curried Noodle - (Single)','description' => 'But may share by two persons','price' => 70.00,'category_id' => 5],
            ['name' => 'Pork Palabok - (Single)','description' => 'But may share by two persons','price' => 60.00,'category_id' => 5],

            ['name' => 'Meke Guisado','description' => 'For 3 persons','price' => 130.00,'category_id' => 5],
            ['name' => 'Fried Noodles','description' => 'For 3 persons','price' => 150.00,'category_id' => 5],
            ['name' => 'Chicken Sotanghon G.','description' => 'For 3 persons','price' => 150.00,'category_id' => 5],
            ['name' => 'Tuna Sotanghon G.','description' => 'For 3 persons','price' => 150.00,'category_id' => 5],

            ['name' => 'Bihon Guisado','description' => 'For 4-5 Persons','price' => 175.00,'category_id' => 5],
            ['name' => 'Pancit Canton','description' => 'For 4-5 Persons','price' => 170.00,'category_id' => 5],
            ['name' => 'Pancit Efuven','description' => 'For 4-5 Persons','price' => 190.00,'category_id' => 5],
            ['name' => 'Bam-i','description' => 'For 4-5 Persons','price' => 180.00,'category_id' => 5],
            ['name' => 'Amanding\'s Special','description' => 'For 4-5 Persons','price' => 200.00,'category_id' => 5],

            ['name' => 'Special Calo-calo Bihon','description' => 'Family 6 Persons','price' => 250.00,'category_id' => 5],
            ['name' => 'Special Pancit Canton','description' => 'Family 6 Persons','price' => 250.00,'category_id' => 5],

            ['name' => 'Hotsilog','description' => ' ','price' => 55.00,'category_id' => 6],
            ['name' => 'Tosilog','description' => ' ','price' => 65.00,'category_id' => 6],
            ['name' => 'Fried Chicken','description' => ' ','price' => 65.00,'category_id' => 6],
            ['name' => 'Pork Chop','description' => ' ','price' => 65.00,'category_id' => 6],

            ['name' => 'Linaga','description' => ' ','price' => 45.00,'category_id' => 7],
            ['name' => 'Stir Fried Beef with Veggies','description' => 'For 2-3 Persons','price' => 130.00,'category_id' => 7],
            ['name' => 'Chop Suey','description' => 'For 2-3 Persons','price' => 100.00,'category_id' => 7],
            ['name' => 'Pork Sigang','description' => 'For 2-3 Persons','price' => 150.00,'category_id' => 7],
            ['name' => 'Rice Per Cup','description' => ' ','price' => 15.00,'category_id' => 7],

            ['name' => 'Fries','description' => ' ','price' => 45.00,'category_id' => 8],
            ['name' => 'Fries Con Carne','description' => ' ','price' => 70.00,'category_id' => 8],
            ['name' => 'Buttered Hotdog','description' => '(3pcs.)','price' => 50.00,'category_id' => 8],
            ['name' => 'Pork Siomai','description' => '(4pcs.)','price' => 35.00,'category_id' => 8],
            ['name' => 'Platu Lumpia','description' => '(6pcs.)','price' => 40.00,'category_id' => 8],

            ['name' => 'Batchoy Mini','description' => 'For dine in only','price' => 30.00,'category_id' => 9],
            ['name' => 'Batchoy Regular','description' => ' ','price' => 43.00,'category_id' => 9],
            ['name' => 'Batchoy Special','description' => ' ','price' => 60.00,'category_id' => 9],
            ['name' => 'Batchoy Chicken','description' => ' ','price' => 60.00,'category_id' => 9],
            ['name' => 'Batchoy Beef','description' => ' ','price' => 65.00,'category_id' => 9],
            ['name' => 'Batchoy Bihon','description' => ' ','price' => 60.00,'category_id' => 9],
            ['name' => 'Batchoy Miswa','description' => ' ','price' => 50.00,'category_id' => 9],
            ['name' => 'Batchoy Meke Veggie','description' => ' ','price' => 55.00,'category_id' => 9],
            ['name' => 'Batchoy Sotanghon Veggie','description' => ' ','price' => 65.00,'category_id' => 9],

            ['name' => 'Sotanghon','description' => ' ','price' => 65.00,'category_id' => 10],
            ['name' => 'Yellow Noodle','description' => ' ','price' => 65.00,'category_id' => 10],

            ['name' => 'Special Lomi','description' => 'For 4 Person','price' => 165.00,'category_id' => 10],
            ['name' => 'Molo Balls Soup','description' => 'For 4 Person','price' => 140.00,'category_id' => 10],
            ['name' => 'Chicken Macaroni Soup','description' => 'For 4 Person','price' => 150.00,'category_id' => 10],

            ['name' => 'Spaghetti','description' => ' ','price' => 55.00,'category_id' => 11],
            ['name' => 'Creamy MExican Beef','description' => ' ','price' => 60.00,'category_id' => 11],
            ['name' => 'Chicken Alfredo','description' => ' ','price' => 60.00,'category_id' => 11],
            ['name' => 'Tuna Carbonara','description' => 'Good for 3 persons','price' => 165.00,'category_id' => 11]

        ]);

    }
}
