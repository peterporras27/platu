<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'admin@cafethalia.com',
            'password' => Hash::make('secret'),
            'role' => 'admin'
        ]);

        User::create([
            'first_name' => 'Delivery',
            'last_name' => 'Rider',
            'email' => 'rider@cafethalia.com',
            'password' => Hash::make('secret'),
            'role' => 'rider'
        ]);

        User::create([
            'first_name' => 'Office',
            'last_name' => 'Staff',
            'email' => 'staff@cafethalia.com',
            'password' => Hash::make('secret'),
            'role' => 'staff'
        ]);
    }
}
