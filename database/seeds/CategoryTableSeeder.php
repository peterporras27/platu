<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            ['name' => 'Loaf'],
            ['name' => 'Buns / Rolls'],
            ['name' => 'Cold Drinks'],
            ['name' => 'Chillers'],
            ['name' => 'Guisado with Love'],
            ['name' => 'Combo Meals with Rice & Sabaw'],
            ['name' => 'Darapli'],
            ['name' => 'Sides'],
            ['name' => 'Batchoy'],
            ['name' => 'Pork Dumpling Soup'],
            ['name' => 'Pasta']
        ]);

    }
}
