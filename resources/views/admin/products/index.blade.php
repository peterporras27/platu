@extends('admin.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Product Listings</h3>
				</div>
				<div class="panel-body">
					
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#prods" aria-controls="prods" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Products</a>
							</li>
							<li role="presentation">
								<a href="#addprods" aria-controls="addprods" role="tab" data-toggle="tab"><i class="fa fa-plus"></i> Add New Product</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="prods"><br>
								@if( $products->count() )

								<div class="table-responsive">
									
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Name</th>
												<th>Price</th>
												<th>Category</th>
												<th>Availability</th>
												<th>Options</th>
											</tr>
										</thead>
										<tbody>
											@foreach($products as $prod)
											<tr class="@if($prod->available) success @else danger @endif">
												<td>{{ $prod->name }}</td>
												<td>₱{{ $prod->price }}</td>
												<td>{{ $prod->category->name }}</td>
												<td>
													@if($prod->available) 
														<span class="badge badge-pill badge-info">
															<i class="fa fa-check"></i> Available 
														</span> 
													@else 
														<span class="badge badge-pill badge-danger">
															<i class="fa fa-close"></i> Out of Stock 
														</span> 
													@endif
												</td>
												<td>
													<a href="{{ route('products.edit',$prod->id) }}" class="btn btn-xs btn-success">Edit <i class="fa fa-edit"></i></a>
													|
													<a href="#" class="btn btn-xs btn-danger">Delete <i class="fa fa-close"></i></a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>

								{!! $products->links() !!}

								@else

								<div class="alert alert-warning">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<strong>Opps!</strong> there are no available products to show at the moment.
								</div>

								@endif

							</div>
							<div role="tabpanel" class="tab-pane" id="addprods">
								<br>
								<div class="row">
									<form enctype="multipart/form-data" action="{{ route('products.store') }}" method="POST" role="form">
									@csrf
									<div class="col-md-6">
										
										<div class="form-group">
											<label for="product-name">Name</label>
											<input type="text" name="name" class="form-control" id="product-name" placeholder="">
										</div>

										<div class="form-group">
											<label for="product-description">Description</label>
											<textarea rows="10" name="description" class="form-control" id="product-description"></textarea>
										</div>

										<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

									</div>
									<div class="col-md-6">

										<div class="row">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="form-group">
													<label for="product-price">Price</label>
													<div class="input-group">
													  <span class="input-group-addon" id="product-price">₱</span>
													  <input type="text" class="form-control" name="price" placeholder="00.00" aria-describedby="product-price">
													</div>
												</div>

												<div class="form-group">
													<label for="product-available">Product Availability</label><br>
													<input type="checkbox" name="available" value="1" class="form-control js-switch" id="product-available" checked> <span class="avail badge badge-pill badge-info">Available</span>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												@if($categories->count())
												<div class="form-group">
													<label for="product-category">Category</label>
													<select name="category_id" class="form-control" id="product-category">
														@foreach($categories as $cat)
														<option value="{{ $cat->id }}">{{ $cat->name }}</option>
														@endforeach
													</select>
												</div>
												@endif	
											</div>
										</div>
										
										<div class="gallery row"></div>
										<div align="center">
											<i id="imgspin" style="display: none;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
										</div>
										<br>
										<div class="form-group">
											<label for="product-photo">Select Photos</label>
											<input type="file" multiple name="photos[]" class="form-control" id="product-photo" placeholder="Select Image">
										</div>
										
									</div>
									
									</form>
								</div>

							</div>
						</div>
					</div>

				</div><!-- panel-body -->
			</div><!-- panel -->


		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
var $=jQuery;
// Multiple images preview in browser
var imagesPreview = function(input, placeToInsertImagePreview) 
{
    if (input.files) 
    {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) 
        {
            var reader = new FileReader();
            reader.onload = function(event) {
            	var r = '<div class="col-md-3 col-sm-3 col-xs-6"><div class="thumbnail" style="width:100%;margin-bottom:15px;height:150px;background:url('+event.target.result+') no-repeat center center;background-size:cover;"></div></div>';
                $($.parseHTML(r)).appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};

$('#product-photo').on('change', function() {
	$('.gallery').html('');
    imagesPreview(this, 'div.gallery');
});

var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function(html) {
  var switchery = new Switchery(html);
});

var changeCheckbox = document.querySelector('.js-switch');
changeCheckbox.onchange = function() {
	if (changeCheckbox.checked) {
		$('.avail').text('Available');
		$('#product-available').val(1);
	} else {
		$('.avail').text('Out of stock');
		$('#product-available').val(0);
	}
};

$(document).ready(function() {
	$('#product-description').summernote({
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
	});	
});
</script>
@endsection