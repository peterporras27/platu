@extends('admin.index')


@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Users</h3>
                </div>
                <div class="panel-body">

                    <a class="btn btn-default" href="{{ route('users.index') }}"><i class="entypo-left-bold"></i> Go Back</a>
                        
                    <hr>
                    @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif

                    <form action="{{ route('users.update', $user->id) }}" method="POST">
                        {{ method_field('PATCH') }}
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                @if($user->valid_id)
                                    <div class="row">
                                        <div class="col-md-4" align="center">
                                            <img src="{{ route('photo', str_replace('uploads/', '', $user->valid_id) ) }}" alt="" class="img-responsive img-thumbnail">
                                            <strong>Valid ID</strong><br>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <strong>First Name:</strong>
                                                <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                            </div>
                                            <div class="form-group">
                                                <strong>Last Name:</strong>
                                                <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                            </div>
                                            <div class="form-group">
                                                <strong>Middle Name:</strong>
                                                <input type="text" class="form-control" name="middle_name" value="{{ $user->middle_name }}">
                                            </div>
                                            <div class="form-group">
                                                <strong>Email:</strong>
                                                <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                                            </div>
                                            <div class="form-group">
                                                <strong>Password:</strong>
                                                <input type="password" class="form-control" name="password" value="" placeholder="Password">
                                            </div>
                                            <div class="form-group">
                                                <strong>Confirm Password:</strong>
                                                <input type="password" class="form-control" name="confirm-password" value="" placeholder="Confirm Password">
                                            </div>
                                        </div>  
                                    </div>
                                @else
                                    <div class="form-group">
                                        <strong>First Name:</strong>
                                        <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                    </div>
                                    <div class="form-group">
                                        <strong>Last Name:</strong>
                                        <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                    </div>
                                    <div class="form-group">
                                        <strong>Middle Name:</strong>
                                        <input type="text" class="form-control" name="middle_name" value="{{ $user->middle_name }}">
                                    </div>
                                    <div class="form-group">
                                        <strong>Email:</strong>
                                        <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                                    </div>
                                    <div class="form-group">
                                        <strong>Password:</strong>
                                        <input type="password" class="form-control" name="password" value="" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <strong>Confirm Password:</strong>
                                        <input type="password" class="form-control" name="confirm-password" value="" placeholder="Confirm Password">
                                    </div>
                                @endif
                                @if( auth()->user()->role == 'admin' )
                                <div class="form-group">
                                    <strong>Role:</strong>
                                    <select class="form-control" name="role">
                                        <option value="customer"{{ $user->role == 'customer' ? ' selected':'' }}>Customer</option>
                                        <option value="staff"{{ $user->role == 'staff' ? ' selected':'' }}>Office Staff</option>
                                        <option value="rider"{{ $user->role == 'rider' ? ' selected':'' }}>Delivery Rider</option>
                                        <option value="admin"{{ $user->role == 'admin' ? ' selected':'' }}>Administrator</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <strong>Verification Code:</strong><br>
                                            <input type="text" name="code" class="form-control" value="{{ $user->code ? $user->code: $code }}"> 
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8">
                                        <div class="form-group">
                                            <strong>Verified Member:</strong><br>
                                            <input type="checkbox" name="verified_member" class="form-control js-switch" value="1" {{ ($user->verified_member) ? 'checked':'' }}> 
                                            <small>&nbsp;Verified members are allowed to checkout and purchase orders.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <strong>Lock Member:</strong><br>
                                            <input type="checkbox" name="banned" class="form-control js-switch" value="1" {{ ($user->banned) ? 'checked':'' }}> 
                                            <small>&nbsp;Banned members are not allowed to checkout and purchase orders.</small>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Street Address:</strong>
                                    <input type="text" class="form-control" name="address" placeholder="" value="{{ $user->address }}">
                                </div>
                                <div class="form-group">
                                    <strong>Town:</strong>
                                    <select name="town_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($towns as $town)
                                        <option value="{{ $town->id }}"{{ $town->id == $user->town_id ? ' selected':'' }}>
                                            {{ $town->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Barangay:</strong>
                                    <select name="barangay_id" class="form-control">
                                        <option value="">Select</option>
                                        @foreach($barangays as $brgy)
                                        <option value="{{ $brgy->id }}"{{ $brgy->id == $user->barangay_id ? ' selected':'' }}>
                                            {{ $brgy->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Phone:</strong>
                                    <input type="text" class="form-control" name="phone" placeholder="0956..." value="{{ $user->phone }}">
                                </div>
                                <div class="form-group">
                                    <strong>Birth Day:</strong>
                                    <input type="date" class="form-control" name="birthday" placeholder="mm/dd/yyy" value="{{ $user->birthday }}">
                                </div>
                                
                                
                            </div>
                            
                        </div>
                        <hr>
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-success btn-lg">Save</button>
                            </div>
                        </div>
                    </form>

                </div><!-- panel-body -->
            </div><!-- panel -->

           
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
$(document).ready(function() {
    var $=jQuery;
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new Switchery(html);
    });

    var changeCheckbox = document.querySelector('.js-switch');
    changeCheckbox.onchange = function() {
        /*if (changeCheckbox.checked) {
            $('.avail').text('Available');
            $('#product-available').val(1);
        } else {
            $('.avail').text('Out of stock');
            $('#product-available').val(0);
        }*/
    };

    $('[name="town_id"]').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        if (id!='') {

            $.ajax({
                url: '{{ route('barangays') }}?id='+id,
                type: 'GET',
                data: {_token: $('[name="token"]').attr('content')},
                success: function (data) {
                    $('[name="barangay_id"]').html('<option value="">Select</option>');
                    if (data.length) {
                        for (var i = 0; i < data.length; i++) {
                            $('[name="barangay_id"]').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                        }
                    }
                }
            });
        }
    });

});
</script>
@endsection