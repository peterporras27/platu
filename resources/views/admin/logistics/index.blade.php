@extends('admin.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Logistics</h3>
				</div>
				<div class="panel-body">
					
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#prods" aria-controls="prods" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Lists</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="prods">
								<br>
								@if($logistics->count())

								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									@foreach($logistics as $town)
									<div class="panel panel-default">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{$town->id}}" aria-expanded="true" aria-controls="collapse-{{$town->id}}">
													{{ $town->name }}
												</a>
											</h4>
										</div>
										<div id="collapse-{{$town->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-striped table-bordered">
														<thead>
															<tr>
																<th>Name</th>
																<th>Delivery Fee</th>
																<th>Options</th>
															</tr>
														</thead>
														<tbody>
															@foreach($town->barangays as $brgy)
															<tr id="brgy-{{ $brgy->id }}">
																<td class="brgy-name">{{ $brgy->name }}</td>
																<td class="brgy-fee">{{ $brgy->delivery_fee }}</td>
																<td>
																	<a href="#" onclick='editCat({{$brgy->id}})' class="btn btn-success btn-xs">Edit <i class="fa fa-edit"></i></a>
																</td>
															</tr>
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									@endforeach
								</div>

								{!! $logistics->render() !!}
								@else
								<div class="alert alert-warning">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<strong>Opps!</strong> there are no logistics to display at the moment.
								</div>
								@endif
							</div>
						</div>
					</div>

				</div><!-- panel-body -->
			</div><!-- panel -->

		</div>
	</div>
</div>
<div class="modal fade" id="edit-cat">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="update-cat-form" action="" method="POST">
				@csrf
				<input type="hidden" name="_method" value="PUT">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Logistics</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="cat-name">Name</label>
						<input type="text" name="name" id="cat-name" class="form-control" value="" required>	
					</div>
					<div class="form-group">
						<label for="cat-delivery_fee">Delivery Fee</label>
						<input type="number" step="0.01" name="delivery_fee" id="cat-delivery_fee" class="form-control" value="" required>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" onclick="saveLogistic()" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
var logistic = {id:'',name:'',delivery_fee:0.00,_method:'PUT',_token:jQuery('#update-cat-form [name="_token"]').val()};
function saveLogistic(){

	logistic.name = jQuery('#cat-name').val();
	logistic.delivery_fee = jQuery('#cat-delivery_fee').val();

	$.ajax({
		url: '{{ route('logistics.index') }}/'+logistic.id,
		type: 'post',
		data: logistic,
		success: function (data) {

			if (data.error) {
				toastr.error('Error', data.message);
			} else {
				toastr.success('Success', data.message);
			}

			jQuery('#brgy-'+logistic.id).find('.brgy-name').text(logistic.name);
			jQuery('#brgy-'+logistic.id).find('.brgy-fee').text(logistic.delivery_fee);
			jQuery('#edit-cat').modal('hide');
		}
	});
}

function editCat(id){

	logistic.id = id;
	logistic.name = jQuery('#brgy-'+id).find('.brgy-name').text();
	logistic.delivery_fee = jQuery('#brgy-'+id).find('.brgy-fee').text();

	jQuery('#update-cat-form').attr('action','{{ route('logistics.index') }}/'+id);
	jQuery('#cat-name').val(logistic.name);
	jQuery('#cat-delivery_fee').val(logistic.delivery_fee);
	jQuery('#edit-cat').modal('show');
}
</script>
@endsection