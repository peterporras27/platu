@extends('admin.index')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Categories</h3>
				</div>
				<div class="panel-body">
					
					<div role="tabpanel">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active">
								<a href="#prods" aria-controls="prods" role="tab" data-toggle="tab"><i class="fa fa-list"></i> Lists</a>
							</li>
							<li role="presentation">
								<a href="#addprods" aria-controls="addprods" role="tab" data-toggle="tab"><i class="fa fa-plus"></i> Create New Category</a>
							</li>
						</ul>

						<!-- Tab panes -->
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="prods">
								<br>
								@if($categories->count())

								<div class="table-responsive">
									
									<table class="table table-striped table-bordered">
										<thead>
											<tr>
												<th>Name</th>
											</tr>
										</thead>
										<tbody>
											@foreach($categories as $cat)
											<tr>
												<td>
													{{ $cat->name }}
													<a href="#" onclick="deleteCat({{$cat->id}})" class="btn btn-danger btn-xs pull-right">Delete <i class="fa fa-close"></i></a>
													<span class="pull-right">&nbsp;|&nbsp;</span>
													<a href="#" onclick='editCat({{$cat->id}},"{{ $cat->name }}")' class="btn btn-success btn-xs pull-right">Edit <i class="fa fa-edit"></i></a>
												</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>

								{!! $categories->render() !!}
								@else
								<div class="alert alert-warning">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<strong>Opps!</strong> there are no categories to display at the moment.
								</div>
								@endif
							</div>
							<div role="tabpanel" class="tab-pane" id="addprods">
								<br>
								<div class="row">
									<form action="{{ route('category.store') }}" method="POST" role="form">
									@csrf
									<div class="col-md-6">
										
										<div class="form-group">
											<label for="product-name">Category Name</label>
											<input type="text" name="name" class="form-control" id="product-name" placeholder="">
										</div>
									
										
										<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

									</div>
									
									</form>
								</div>

							</div>
						</div>
					</div>

				</div><!-- panel-body -->
			</div><!-- panel -->

		</div>
	</div>
</div>
<div class="modal fade" id="edit-cat">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="update-cat-form" action="" method="POST">
				@csrf<input type="hidden" name="_method" value="PUT">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Category</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="cat-name">Name</label>
						<input type="text" name="name" id="cat-name" class="form-control" value="" required>	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<form id="delete-cat-form" action="" method="POST" style="display: none;">
@csrf<input type="hidden" name="_method" value="DELETE"></form>
@endsection
@section('scripts')
<script>
function deleteCat(id)
{
	jQuery('#delete-cat-form').attr('action','{{ route('category.index') }}/'+id);
	document.getElementById('delete-cat-form').submit();
}

function editCat(id,val){
	jQuery('#update-cat-form').attr('action','{{ route('category.index') }}/'+id);
	jQuery('#cat-name').val(val);
	jQuery('#edit-cat').modal('show');
}
</script>
@endsection