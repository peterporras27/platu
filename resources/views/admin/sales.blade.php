@extends('admin.index')

@section('styles')
<style>
select[name="month"] {
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
	border-right: 0;
}
input[name="year"] {
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">
						Total Sales For 
						@if(request()->get('date'))
							{{ date('F j, Y',strtotime(request()->get('date'))) }}
						@else
							Today
						@endif
					</h3>
				</div>
				<div class="panel-body">
					
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#daily" aria-controls="daily" role="tab" data-toggle="tab">Daily</a></li>
						<li role="presentation"><a href="#monthly" aria-controls="monthly" role="tab" data-toggle="tab">Monthly</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="daily">
							<br>
							<div class="row">
								<div class="col-md-4">
									<form method="GET">
										<div class="input-group">
											<input type="date" name="date" value="{{ request()->get('date') }}" class="form-control">
											<span class="input-group-btn">
												<button class="btn btn-default" type="submit">GO</button>
											</span>
										</div>
									</form>		
								</div>
							</div>
							<hr>
							@if($sales->count())
								<div class="table-responsive">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Name</th>
												<th>Transaction Code</th>
												<th>Quantity</th>
												<th>Price</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody>
											@foreach($sales as $order)
												<?php 
												$count = 0; 
												$code = strtoupper($order->transaction_code); 
												$carts = $order->carts();
												?>

												@foreach($carts as $cart)
												<tr>
													<td>{{ $cart->product_name }}</td>
													<td>
														<a class="btn btn-default btn-xs" href="{{ route('transaction.edit',$order->id) }}">{{ $code }}</a>
													</td>
													<td>{{ $cart->quantity }}</td>
													<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
													<td>₱{{ number_format( ($cart->price*$cart->quantity) ,2,'.',',') }}</td>
												</tr>
												<?php 
												$total = $total+$cart->price; 
												$count = $count+$cart->quantity; ?>
												@endforeach	
												<?php 
												$fee = 200;
										        if ($order->province=='mina') {
										            $fee = 50;
										            if (in_array($order->barangay, $barangay)) {
										                //$fee = 50;
										            }
										        }

										        $total = $total+$fee;

										        $monthly[$order->id] = [
										        	'transaction_code' => $code,
										        	'address' => $order->address,
										        	'phone' => $order->phone,
										        	'name' => $order->first_name.' '.$order->last_name,
										        	'date' => $order->created_at->format('F j, Y'),
										        	'total' => $total
										        ];
												?>
												<tr>
													<td>Delivery Fee</td>
													<td><a class="btn btn-default btn-xs" href="{{ route('transaction.edit',$order->id) }}">{{ $code }}</a></td>
													<td>{{ $order->barangay.', '.ucwords($order->province) }}, Iloilo City</td>
													<td>₱{{ number_format( $fee,2,'.',',') }}</td>
													<td>₱{{ number_format( $fee,2,'.',',') }}</td>
												</tr>
											@endforeach
											<tr class="success">
												<td><b>TOTAL:</b></td>
												<td></td>
												<td></td>
												<td></td>
												<td><b class="grand">₱{{ number_format(($total),2,'.',',') }}</b></td>
											</tr>
										</tbody>
									</table>
								</div>
							@else
								<div class="alert alert-info">
									<strong>Oops</strong> There are no records to show at the moment.
								</div>
							@endif
						</div>
						<div role="tabpanel" class="tab-pane" id="monthly">
							<br>
							<div class="row">
								<div class="col-md-4">
									<form method="GET">
										<div class="row">
											<div class="col-md-4" style="padding-right:0;">
												<select name="month" class="form-control">
													<option value="01"{{ $month == '01' ? ' selected':'' }}>January</option>
													<option value="02"{{ $month == '02' ? ' selected':'' }}>Febuary</option>
													<option value="03"{{ $month == '03' ? ' selected':'' }}>March</option>
													<option value="04"{{ $month == '04' ? ' selected':'' }}>April</option>
													<option value="05"{{ $month == '05' ? ' selected':'' }}>May</option>
													<option value="06"{{ $month == '06' ? ' selected':'' }}>June</option>
													<option value="07"{{ $month == '07' ? ' selected':'' }}>July</option>
													<option value="08"{{ $month == '08' ? ' selected':'' }}>August</option>
													<option value="09"{{ $month == '09' ? ' selected':'' }}>September</option>
													<option value="10"{{ $month == '10' ? ' selected':'' }}>October</option>
													<option value="11"{{ $month == '11' ? ' selected':'' }}>November</option>
													<option value="12"{{ $month == '12' ? ' selected':'' }}>December</option>
												</select>
											</div>
											<div class="col-md-4" style="padding-left:0;">
												<div class="input-group">
													<input type="number" name="year" class="form-control" value="{{ $year }}" placeholder="year">
													<span class="input-group-btn">
														<button class="btn btn-default" type="submit">GO</button>
													</span>
												</div>
											</div>
										</div>
									</form>	
								</div>
							</div>
							<hr>
							
							@if(count($monthly))
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>Transaction Code</th>
											<th>Address</th>
											<th>Phone</th>
											<th>Name</th>
											<th>Date</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										@foreach($monthly as $monthid => $month)
										<tr>
											<td>{{ $month['transaction_code'] }}</td>
								        	<td>{{ $month['address'] }}</td>
								        	<td>{{ $month['phone'] }}</td>
								        	<td>{{ $month['name'] }}</td>
								        	<td>{{ $month['date'] }}</td>
								        	<td>₱{{ number_format(($month['total']),2,'.',',') }}</td>
								        </tr>
								        <?php $grand_total = $grand_total+$month['total']; ?>
										@endforeach
										<tr class="success">
											<td><b>GRAND TOTAL:</b></td>
								        	<td></td>
								        	<td></td>
								        	<td></td>
								        	<td></td>
								        	<td><b class="grand">₱{{ number_format(($grand_total),2,'.',',') }}</b></td>
										</tr>
									</tbody>
								</table>
							</div>
							@else
							<div class="alert alert-info">
								<strong>Oops</strong> There are no records to show at the moment.
							</div>
							@endif
						</div>
					</div>
					
				</div>
			</div>

		</div>
	</div>	
</div>
@endsection