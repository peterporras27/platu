<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{ csrf_token() }}">
        <title>Platu Cafe</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/carousel.css')}}" rel="stylesheet">
        <style>
        .navbar-inverse{border-color: #5b3a1b;}
        .navbar-inverse .navbar-toggle{border-color: #5b3a1b;}
        .mainTitle {border-top: 1px solid #5b3a1b;}
        .navbar-inverse .navbar-toggle .icon-bar{background-color: #5b3a1b;}
        .navbar-inverse .navbar-brand{color:#5b3a1b; }
        .navbar-brand{padding: 4px 10px;}
        .navbar-brand img{width: 135px;}

        @media(max-width: 767px) {
            .floating {
                position: unset;
                padding: 0 15px;
            }

            .floating img {
                max-width: 100%;
            }

            h2{font-size: 16px;}

            .cat {min-height: 100px;margin-bottom: 15px;}
            .cat .img{ height: 100px; }

            .products .btn-danger {
                font-size: 10px;
            }
        }
        </style>
        @yield('header')
    </head>
    <body>
        <nav class="navbar navbar-inverse" style="border-radius: 0;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ asset('images/platu.png') }}" class="img-responsive" alt="">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li @if(Route::current()->getName() == 'home') class="active" @endif><a href="{{ route('home') }}">Home</a></li>
                        @if (Route::has('login'))
                            @auth
                                <?php $userroles = ['admin','staff']; ?>
                                @if( in_array(auth()->user()->role, $userroles) )
                                <li @if(Route::current()->getName() == 'admin') class="active" @endif><a href="{{ route('admin') }}">Admin Panel</a></li>
                                @endif
                                @if( auth()->user()->role != 'rider' )
                                    <li @if(Route::current()->getName() == 'orders') class="active" @endif><a href="{{ route('orders.index') }}">Orders</a></li>
                                    <li id="cart-nav" @if(Route::current()->getName() == 'cart') class="active" @endif><a href="{{ route('cart.index') }}"><i class="glyphicon glyphicon-shopping-cart"></i> Cart (<span>{{auth()->user()->cart()}}</span>)</a></li>
                                @endif
                            @else
                                <li @if(Route::current()->getName() == 'login') class="active" @endif><a href="{{ route('login') }}">Login</a></li>
                                @if (Route::has('register'))
                                    <li @if(Route::current()->getName() == 'register') class="active" @endif><a href="{{ route('register') }}">Register</a></li>
                                @endif
                            @endauth
                        @endif
                        <li>
                            <a href="tel:09617588397"><i class="glyphicon glyphicon-phone"></i>0961 758 8397</a>
                        </li>
                    </ul>
                    @auth
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{ route('settings.index') }}">
                                {{ __('Settings') }}
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                        </li>
                    </ul>
                    @endauth
                </div>
            </div>
        </nav>
        
        @yield('content')

        <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/toastr.min.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        @yield('footer')
        @if(session('success'))
            <script>toastr.success('{{ session('success') }}', 'Success!')</script>
        @endif
        @if(session('error'))
            <script>toastr.error('{!! session('error') !!}', 'Error!')</script>
        @endif
        @if ($errors->any())
        <script>
            @foreach ($errors->all() as $err)
                toastr.error('{{ $err }}', 'Error!');
            @endforeach
        </script>
        @endif
    </body>
</html>