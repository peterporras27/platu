@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
</style>
@endsection
@section('content')

<div class="mainTitle">
    <div class="container">
        <h1>Orders Details</h1>
    </div>
</div>

<div class="container-fluid marketing">
    <div>
        
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Transactions</h3>
                </div>
                <div class="panel-body">
                    
                    <div class="row">
						<div class="col-md-6">
							<form action="{{ route('rider.update', $trans->id) }}" enctype="multipart/form-data" method="POST">
								{{ method_field('PATCH') }}
            					@csrf
								<div class="table-responsive">
									<table class="table table-hover table-bordered">
										<thead>
											<tr>
												<th>Name</th>
												<th>Details</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Transaction ID:</td>
												<td>{{ strtoupper($trans->transaction_code) }}</td>
											</tr>
											<tr>
												<td>Name:</td>
												<td>{{ $trans->first_name.' '.$trans->last_name }}</td>
											</tr>
											<tr>
												<td>Address:</td>
												<td>{{ $trans->address }}</td>
											</tr>
											<tr>
												<td>Phone:</td>
												<td>{{ $trans->phone }}</td>
											</tr>
											<tr>
												<td>Delivery Rider:</td>
												<td>
													@if($trans->rider)
														{{ $trans->rider->first_name.' '.$trans->rider->last_name }} ({{ $trans->rider->username }})
													@endif
												</td>
											</tr>
											<tr>
												<td>Order Status:</td>
												<td>
		                        					<select class="form-control" name="status">
	                        							<option value="delivered"{{ $trans->status=='delivered' ? ' selected':'' }}>Delivered</option>
	                        							<option value="failed"{{ $trans->status=='failed' ? ' selected':'' }}>Canceled / Failed</option>
	                        						</select>
												</td>
											</tr>
											@if($trans->status!='failed')
											<tr>
												<td>Proof of claim:</td>
												<td>
													@if($trans->proof)
													<img src="{{ route('photo', str_replace('uploads/', '', $trans->proof) ) }}" class="img-responsive img-thumbnail">
													@endif

													@if( auth()->user()->role=='rider' && $trans->status=='delivery' )
													<input type="file" name="proof" class="form-control">
													@endif
												</td>
											</tr>
											@endif
											<tr id="reason" style="{{ $trans->status=='failed' ? '':'display:none;' }}">
												<td>Reason:</td>
												<td>
													<textarea name="reason" class="form-control" rows="5" placeholder="Indicate reason...">{{ $trans->reason }}</textarea>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>
							</form>
						</div>
						<div class="col-md-6">
							<div class="table-responsive">
								<table class="table table-hover table-bordered">
									<thead>
										<tr>
											<th>Name</th>
											<th>Quantity</th>
											<th>Price</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<?php $total = 0; $count = 0; ?>
										@foreach($carts as $cart)
										<tr>
											<td>{{ $cart->product_name }}</td>
											<td>{{ $cart->quantity }}</td>
											<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
											<td>₱{{ number_format( ($cart->price*$cart->quantity) ,2,'.',',') }}</td>
										</tr>
										<?php $total = $total+$cart->price; $count = $count+$cart->quantity; ?>
										@endforeach	
										<tr>
											<td>Delivery Fee</td>
											<td></td>
											<td></td>
											<td>₱{{ number_format( $fee,2,'.',',') }}</td>
										</tr>
										<tr class="success">
											<td><b>TOTAL:</b></td>
											<td class="quantity">{{$count}}</td>
											<td></td>
											<td><b class="grand">₱{{ number_format(($total+$fee),2,'.',',') }}</b></td>
										</tr>
									</tbody>
								</table>
							</div>

							@if($trans->status=='delivered')
							<a href="{{ route('receipt',$trans->id) }}" target="_blank" class="btn btn-default">View Recipt <i class="fa fa-eye"></i></a>
							@endif
						</div>
					</div>

                </div>
            </div>
            
        </div>
        
    </div>
</div>

@endsection
@section('footer')

@endsection