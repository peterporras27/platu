<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="token" content="{{ csrf_token() }}">
        <title>Platu Cafe</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/carousel.css')}}" rel="stylesheet">
        @yield('header')
        <style type="text/css">
            .login-btn{border-color: #603913;background-color: #603913;}
        </style>
    </head>
    <body>
        <br><br>
        <div class="container">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4" align="center">
                    <img class="img-responsive" style="max-width: 300px;" src="{{ asset('images/logo.jpg') }}" alt="">
                    <div>
                        <a style="background-color: #603913;"   href="{{ route('login') }}" class="login-btn btn btn-success btn-block btn-lg">LOGIN</a>
                        <br>
                        <a style="background-color: #603913;" href="{{ route('register') }}" class="login-btn btn btn-success btn-block btn-lg">REGISTER</a>
                        <br>
                    </div>
                        <h5>"We'd love to hear your feedback"</h5>
                        <i class="glyphicon glyphicon-phone"></i>&nbsp;0961 758 8397
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        

        <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/toastr.min.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        @yield('footer')
        @if(session('success'))
            <script>toastr.success('{{ session('success') }}', 'Success!')</script>
        @endif
        @if(session('error'))
            <script>toastr.error('{{ session('error') }}', 'Error!')</script>
        @endif
    </body>
</html>