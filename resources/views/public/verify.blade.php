@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container">
		<h1>Verify Account Membership</h1>
	</div>
</div>

<div class="container marketing">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Account Verification</h3>
				</div>
				<div class="panel-body">
					<form action="{{ route('verify_user') }}" enctype="multipart/form-data" method="POST" role="form">
						@csrf
						<div class="form-group">
							<label>Enter Verification Code:</label>
							<input type="text" name="code" value="{{ $code }}" class="form-control" placeholder="">
						</div>
						<div class="form-group">
							<label>Valid ID:</label>
							<input type="file" name="valid_id" class="form-control">
							<small>Upload any valid government or company ID.</small>
						</div>
						<button type="submit" class="btn btn-success btn-block">Verify Account <i class="glyphicon glyphicon-ok"></i></button>
						<div align="center">
							<a href="{{ route('send_code') }}" class="btn btn-link"><small>Send Email Verification Code</small></a>
						</div>

					</form>

				</div>
			</div>
			
		</div>
		<div class="col-md-4"></div>
	</div>
</div>

@endsection
@section('footer')
<script>
var $=jQuery;
</script>
@endsection