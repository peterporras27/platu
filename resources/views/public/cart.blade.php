@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
.tab-content{
	padding: 15px;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;

    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px;
    border-top-right-radius: 5px;
}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container" align="center">
		<h1>Cart Order Information</h1>
	</div>
</div>

<div class="container marketing">

	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active">
				<a href="#home" aria-controls="home" role="tab" data-toggle="tab" style="color: #000;">Checkout Information</a>
			</li>
		</ul>
	
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">

				<form id="checkout-form" action="{{ route('cart.store') }}" method="POST" role="form">
					@csrf
					<input type="hidden" name="payment_type" value="cash_on_delivery">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name:</label>
								<input type="text" name="first_name" class="form-control" value="{{ old('first_name') ? old('first_name') : auth()->user()->first_name }}" required>
							</div>
							<div class="form-group">
								<label>Last Name:</label>
								<input type="text" name="last_name" class="form-control" value="{{ old('last_name') ? old('last_name') : auth()->user()->last_name }}" required>
							</div>
							<div class="form-group">
								<label>Phone:</label>
								<input type="text" name="phone" class="form-control" value="{{ old('phone') ? old('phone') : auth()->user()->phone }}" required>
							</div>
						</div>
						<div class="col-md-6">
							 
							<input type="hidden" name="address" class="form-control" value="{{ old('address') ? old('address') : auth()->user()->address }}" required>
							<div class="form-group">
								<label>Street Address:</label>
								<input id="address" name="address" type="text" value="{{ auth()->user()->address }}" class="form-control">
							</div>
							<div class="form-group">
								<label>Town:</label>
								<select id="town" name="town_id" class="form-control">
									<option value="">Select</option>
									@foreach($towns as $town)
									<option value="{{$town->id}}"{{ $town->id == auth()->user()->town_id ? ' selected':'' }}>
										{{$town->name}}
									</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Barangay:</label>
								<select id="barangay" name="barangay_id" class="form-control">
									<option value="">Select</option>
									@if($barangays)
									@foreach($barangays as $brgy)
									<option data-fee="{{ $brgy->delivery_fee }}" value="{{$brgy->id}}"{{ $brgy->id == auth()->user()->barangay_id ? ' selected':'' }}>
										{{$brgy->name}}
									</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<label>Order Notes:</label>
						<textarea rows="3" name="notes" class="form-control" placeholder="Special requests etc."></textarea>
					</div>
				</form>
				<hr>
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						<thead>
							<tr class="info">
								<th>Option</th>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $total = 0; $count = 0; ?>
							@foreach($carts as $cart)
								<tr>
									<td>
										<form action="{{ route('cart.destroy', $cart->id) }}" method="POST" style="display:inline;">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-xs">Remove <i class="glyphicon glyphicon-remove"></i></button>
                                        </form>
									</td>
									<td>{{ $cart->product_name }}</td>
									<td>
										<input onchange="cartUpdate(this,{{$cart->product_id}})" data-price="{{ $cart->price }}" type="number" min="1" value="{{ $cart->quantity }}">
									</td>
									<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
									<td id="total-{{$cart->product_id}}">₱{{ number_format(($cart->price*$cart->quantity),2,'.',',') }}</td>
								</tr>
								<?php 
								$prodprice = $cart->price*$cart->quantity;
								$total = $total+$prodprice; 
								$count = $count+$cart->quantity; ?>
							@endforeach	
							<tr>
								<td><b>Delivery Fee</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td><span id="fee">₱100.00</span></td>
							</tr>
							<tr class="success">
								<td><b>TOTAL:</b></td>
								<td></td>
								<td class="quantity">{{$count}}</td>
								<td></td>
								<td><b class="grand">₱{{ number_format($total,2,'.',',') }}</b></td>
							</tr>
						</tbody>
					</table>
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Note:</strong> No cancelation or changes allowed. If the order was not accepted or if cancelled, account will automatically be blocked on our online delivery system. 
					</div>
				</div>

				<button onclick="checkout('cash_on_delivery')" class="btn btn-success btn-checkout">
					Cash On Delivery <i class="glyphicon glyphicon-ok"></i>
				</button>
				<button onclick="checkout('gcash')" class="btn btn-primary btn-checkout">
					Pay Using <img src="{{ asset('images/gcash.png') }}" alt="Gcash" style="width:70px;">
				</button>
			
			</div>
			
		</div>
	</div>


</div>

@endsection
@section('footer')
<script>
var $=jQuery;
var overall = {{ number_format($total,2,'.','') }};
var delivery_fee = 100;

function checkout(type) {
	$('.btn-checkout').prop('disabled',true);
	$('[name="payment_type"]').val(type);
	$('#checkout-form').submit();
}

function cartUpdate(input,id){

	var quantity = parseInt($(input).val());
	var price = $(input).data('price');

	var formatter = new Intl.NumberFormat('en-PH', {
	  style: 'currency',
	  currency: 'PHP',

	  // These options are needed to round to whole numbers if that's what you want.
	  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});

	$('#total-'+id).html( formatter.format((parseFloat(price)*quantity)) );

	$.ajax({
		url: '{{route('add')}}',
		type: 'POST',
		dataType: 'json',
		data: {id:id,_token:'{{csrf_token()}}',quantity:quantity},
	}).always(function(res) {

		var total = 0;
		var grandTotal = 0;
		$('[type="number"]').each(function(index, el) {
			var quantity = parseInt($(this).val());
			var price = $(this).data('price');
			total = parseInt(total)+quantity;
			grandTotal = grandTotal+(parseFloat(price)*quantity);
		});

		$('.quantity').text(total);
		overall = grandTotal;
		grandTotal = parseFloat(delivery_fee)+parseFloat(grandTotal);
		$('.grand').text(formatter.format(grandTotal));
		if (res.error) {

			toastr.warning('Oops! something went wrong.', 'Error!');

		} else {

			toastr.success('Cart Updated!', 'Success!');
		}
	});
}

function totalallPrices()
{
	var formatter = new Intl.NumberFormat('en-PH', {
	  style: 'currency',
	  currency: 'PHP',
	  // These options are needed to round to whole numbers if that's what you want.
	  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});

	delivery_fee = $('[name="barangay_id"]').find(":selected").data('fee');

	var total = parseFloat(overall)+parseFloat(delivery_fee);
	$('.grand').text( formatter.format(total) );
	$('#fee').text(formatter.format(delivery_fee));
	console.log(delivery_fee);
}

$(document).ready(function() {

	totalallPrices();

	$('[name="town_id"]').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        if (id!='') {

            $.ajax({
                url: '{{ route('barangays') }}?id='+id,
                type: 'GET',
                data: {_token: $('[name="token"]').attr('content')},
                success: function (data) {
                    $('[name="barangay_id"]').html('<option value="">Select</option>');
                    if (data.length) {
                        for (var i = 0; i < data.length; i++) {
                            $('[name="barangay_id"]').append('<option data-fee="'+data[i].delivery_fee+'" value="'+data[i].id+'">'+data[i].name+'</option>');
                        }
                    }
                }
            });
        }
    });

    $('[name="barangay_id"]').change(function (e) {
        e.preventDefault();
        delivery_fee = $(this).find(':selected').data('fee');
        totalallPrices();
    });

});
</script>
@endsection