@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container">
		<h1>Account Settings</h1>
	</div>
</div>

<div class="container marketing">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Details</h3>
				</div>
				<div class="panel-body">

					<form action="{{ route('settings.update', $user->id) }}" method="POST">
                        {{ method_field('PATCH') }}
                        @csrf
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>First Name:</strong>
                                    <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                </div>
                                <div class="form-group">
                                    <strong>Last Name:</strong>
                                    <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                </div>
                                <div class="form-group">
                                    <strong>Middle Name:</strong>
                                    <input type="text" class="form-control" name="middle_name" value="{{ $user->middle_name }}">
                                </div>
                                <div class="form-group">
                                    <strong>Email:</strong>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                                </div>
                                <div class="form-group">
                                    <strong>Password:</strong>
                                    <input type="password" class="form-control" name="password" value="" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <strong>Confirm Password:</strong>
                                    <input type="password" class="form-control" name="confirm-password" value="" placeholder="Confirm Password">
                                </div>
                                
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <strong>Address:</strong>
                                    <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                                </div>
                                <div class="form-group">
                                    <strong>Town:</strong>
                                    <select class="form-control" name="town_id">
                                        <option value="">Select</option>
                                        @foreach($towns as $town)
                                        <option value="{{ $town->id }}"{{ $user->town_id == $town->id ? ' selected':'' }}>
                                            {{ $town->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Barangay:</strong>
                                    <select class="form-control" name="barangay_id">
                                        <option value="">Select</option>
                                        @foreach($baragays as $brgy)
                                        <option value="{{ $brgy->id }}"{{ $user->barangay_id == $brgy->id ? ' selected':'' }}>
                                            {{ $brgy->name }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <strong>Phone:</strong>
                                    <input type="text" class="form-control" name="phone" placeholder="0956..." value="{{ $user->phone }}">
                                </div>
                                <div class="form-group">
                                    <strong>Birth Day:</strong>
                                    <input type="date" class="form-control" name="birthday" placeholder="mm/dd/yyy" value="{{ $user->birthday }}">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-success btn-lg">Save <i class="glyphicon glyphicon-floppy-saved"></i></button>
                            </div>
                        </div>
                    </form>
				</div>
			</div>
			
		</div>
		
	</div>
</div>

@endsection
@section('footer')
<script>
var $=jQuery;
$(document).ready(function(){
    $('[name="town_id"]').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        console.log(id);

        if (id!='') {

            $.ajax({
                url: '{{ route('barangays') }}?id='+id,
                type: 'GET',
                data: {_token: $('[name="token"]').attr('content')},
                success: function (data) {
                    $('[name="barangay_id"]').html('<option value="">Select</option>');
                    if (data.length) {
                        for (var i = 0; i < data.length; i++) {
                            $('[name="barangay_id"]').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                        }
                    }
                }
            });
        }
    });
});
</script>
@endsection