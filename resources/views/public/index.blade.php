@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 500px;}
.cat .thumbnail{
	overflow: hidden;
}
.cat .img{
	height: 300px;
	background-size:cover !important;
}
.floating {
	position: fixed;
	left: 15px;
	z-index: 9;
	width: 16%;
}
.floating img {
	margin:  10px 0;
}

@media(max-width: 767px) {
	.floating {
		position: unset;
		width: 100%;
	}

	.cat .img{
		height: 100px;
	}

	.cat{
		min-height: 220px;
	}

	.products .btn-default{
		font-size: 10px;
	}
}
</style>
@endsection
@section('content')

<div class="mainTitle">

	<div class="floating" align="center">
		<img class="img-responsive" style="max-width: 200px;" src="{{ asset('images/logo.jpg') }}" alt="">
	  	@foreach($categories as $category)
			<a href="#" onclick="showHide(event,{{ $category->id }})" class="btn btn-default" style="margin-bottom: 5px;">{{ $category->name }}</a>
		@endforeach
	</div>

	<div class="container-fluid marketing">

		
		<div class="col-md-2 col-sm-4" align="center"></div>
		<div class="col-md-10 col-sm-8">
			
			<div class="products">
				<div class="row">
					@foreach($products as $product)

						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 cat cat-{{$product->category_id}}">
							
							<div class="thumbnail">
							@if($product->images->first())
								<div class="img" style="background:url('{{ route('image',$product->images->first()->id) }}') no-repeat center center;"></div>
							@else
								<div class="img" style="background:url('{{asset('assets/images/placeholder.png')}}') no-repeat center center;"></div>
							@endif
							</div>
							
							<h2>{{$product->name}}</h2>
							<div class="description">{!! $product->description !!}</div>
							<p><button onclick="addToCart({{$product->id}})" class="btn btn-default">₱{{$product->price}} Add to cart &raquo;</button></p>
						</div>

					@endforeach	
				</div>
			</div>

		</div>

	</div>

</div>


@endsection
@section('footer')
<script>
var $=jQuery;
function showHide(e,id){
	e.preventDefault();
	var $=jQuery;
	
	$('.cat').hide();
	$('.cat-'+id).fadeIn();
}
function addToCart(id){

	$.ajax({
		url: '{{route('add')}}',
		type: 'POST',
		dataType: 'json',
		data: {id:id,_token:'{{csrf_token()}}',quantity:1},
	}).always(function(res) {

		if (res.error) {

			toastr.warning(res.message, 'Error!');

		} else {

			toastr.success(res.message, 'Success!');
			$('#cart-nav').find('span').text(res.count);
		}
	});
}
</script>
@endsection